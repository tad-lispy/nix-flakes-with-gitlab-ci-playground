# Default settings for Make

# Always use strict bash
SHELL := bash
.SHELLFLAGS := -euvo pipefail -c
.ONESHELL:
.SILENT:

# If a recipe fails, delete it's targets to avoid broken state
.DELETE_ON_ERROR:

# Warn about undefined variables and
# TODO: How to make it an error, like in bash strict mode?
# TODO: Consider https://www.artificialworlds.net/blog/2015/04/22/treat-warnings-as-errors-in-a-gnu-makefile/
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --warn-undefined-functions

# Remove implicit rules - no magic please
# See https://www.gnu.org/software/make/manual/html_node/Catalogue-of-Rules.html
MAKEFLAGS += --no-builtin-rules

help: ## Print this help message
help: .SHELLFLAGS := -euo pipefail -c
help:
	@echo "Useage: make [ goals ]"
	@echo
	@echo "Available goals:"
	@echo
	@cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help
