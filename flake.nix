{
  description = "GitLab CI Playground";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-compat, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        shared-inputs = [
          pkgs.gnumake
        ];
      in rec {
        packages.gitlab-ci-playground = pkgs.stdenv.mkDerivation {
          name = "gitlab-ci-playground";
          src = self;
          buildInputs = shared-inputs ++ [
          ];
        };

        devShell = pkgs.mkShell {
          name = "gitlab-ci-playground-development-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.cachix
            pkgs.jq
          ];
        };

        defaultPackage = packages.gitlab-ci-playground;
      }
    );
}
