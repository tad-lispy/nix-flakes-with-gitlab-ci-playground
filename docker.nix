{ pkgs ? import <nixpkgs> { system = "x86_64-linux"; } }:

pkgs.dockerTools.buildImage {
  name = "hello-docker";
  config = {
    Cmd = [
      "${pkgs.nixFlakes}/bin/nix"
      "--experimental-features"
      "nix-command flakes"
      "shell"
      "nixpkgs#hello"
      "--command"
      "hello"
    ];


  };
}
